<?php
// https://www.php.net
// ceci est un commentaire
# ceci est aussi un commentaire (utilisez plutot l'autre syntaxe)

/*
ceci
est
un commentaire
multilignes
 */


$largeur_2 =            /* commentaire */12;
$largeur_3 =     $largeur_2; // =>12

// number
$nombre = 0.5;
$nombre2 = -1;

// string
$nom = "Lefebvre";
$prenom = "William";

// boolean
$boolean = true;
$boolean2 = false;



//$resultat = 1 * 2;

$produit = $nombre * $nombre2;
$addition = $nombre + $nombre2;
$soustraction = $nombre - $nombre2;
$division = $nombre / $nombre2;

$modulo = $nombre % $nombre2;
$exponentiel = $nombre ** $nombre2;


$nomComplet = $nom . $prenom;
$nomCompletAvecEspace = $nom . " " . $prenom;

$calcul = ($nombre + $nombre2) / 2 * 3;

$variable = 5 . 3;


$temps = 0;
$temps = $temps + 1; // 0 + 1 = 1
$temps = $temps + 1; // 1 + 1 = 2
$temps = $temps + 1; // 2 + 1 = 3

$temps += 1; // $temps = $temps + 1;  4
$temps -= 1; // $temps = $temps - 1;  3
$temps /= 2; // $temps = $temps / 2;  1.5
$temps *= 2; // $temps = $temps * 2; 3
$temps .= 1; // $temps = $temps . 1; 31

$temps++; // $temps = $temps + 1; => incrementer
$temps--; // $temps = $temps - 1; => decrementer

//echo $variable;
//echo $nomCompletAvecEspace;


$age = 18;

$majeur = $age >= 18;

//echo $majeur;
//var_dump($majeur);


$comparaison = $age == 15;
$comparaison = $age != 15;
$comparaison = $prenom == "william";
$comparaison = $prenom != "william";

$comparaison = $age > 15;
$comparaison = $age < 15;

$comparaison = $age >= 10;
$comparaison = $age <= 10;

$comparaison = $age === 18; // même valeure et même type
$comparaison = $age !== 18; // valeur differente ou type different


//var_dump("william" == "William"); // faux (comparaison sensible à la casse)

var_dump($age == "18"); // => vrai
var_dump($age === "18"); // => faux (pas le même type)

$sexe = "Femme";

$comparaison = $age > 18 && $sexe === "Femme";

$majeurajeur = $age > 18;
$estUneFemme = $sexe === "Femme";

$comparaison = $majeur && $estUneFemme; // => et
$comparaison = $majeur and $estUneFemme;

$comparaison = $majeur || $majeur; // => ou
$comparaison = $majeur or $majeur;


$comparaison = !$majeur;

$age = 18;
$majeur = $age>=18;
/*
if($majeur){
    echo "Vous êtes majeur!!";
}

if(!$majeur){
    echo "Vous êtes mineur!!";
}*/


if($majeur) {
    echo "Vous êtes majeur!!";
} else {
    echo "Vous êtes mineur!!";
}

if($age > 18){
  echo "Vous êtes majeur!!";
} elseif ($age == 18) {
  echo "Vous êtes tout juste majeur!!";
} else {
  echo "Vous êtes mineur!!";
}
